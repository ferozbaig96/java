package com.kennycason.genetic.draw;

import java.util.ArrayList;

public class ProducerConsumerTest {

    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        Producer producer = new Producer(arrayList, 5);
        Consumer consumer = new Consumer(arrayList, 5);

        new Thread(producer).start();
        new Thread(consumer).start();
    }
}
