package com.kennycason.genetic.draw;

import java.util.ArrayList;
import java.util.Random;

public class Producer implements Runnable {

    ArrayList<Integer> arrayList;
    int capacity;
    Random random;

    Producer(ArrayList arrayList, int capacity) {
        this.arrayList = arrayList;
        this.capacity = capacity;
        random = new Random();
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (arrayList.size() == capacity) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                int x = random.nextInt();
                arrayList.add(x);
                System.out.println("Produced!!! : " + x);
                notifyAll();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
