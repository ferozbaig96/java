package com.kennycason.genetic.draw;

import java.util.ArrayList;
import java.util.Random;

public class Consumer implements Runnable {

    ArrayList<Integer> arrayList;
    int capacity;
    Random random;

    Consumer(ArrayList arrayList, int capacity) {
        this.arrayList = arrayList;
        this.capacity = capacity;
        random = new Random();
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (arrayList.size() == 0) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                int x = arrayList.remove(arrayList.size() - 1);
                System.out.println("Consuming : " + x);
                notifyAll();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
