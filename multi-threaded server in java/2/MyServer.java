package com.kennycason.genetic.draw;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer implements Runnable {

    private ServerSocket serverSocket;
    private Thread currentThread;
    private int port = 8080;
    private boolean hasStopped;

    private MyServer(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        synchronized (this) {
            this.currentThread = Thread.currentThread();
        }
        openServerSocket();

        while (!hasStopped()) {
            Socket clientSocket = null;
            try {
                clientSocket = this.serverSocket.accept();
                System.out.println("Connected!");
            } catch (IOException e) {
                e.printStackTrace();

                if (hasStopped)
                    System.out.println("Server has stopped");
                else
                    System.out.println("Client cannot be connected");
                return;
            }

            new Thread(
                    new MyWorkerRunnable(clientSocket, "HEYA")
            ).start();
        }
        System.out.println("Server has stopped");

    }

    private synchronized boolean hasStopped() {
        return hasStopped;
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cannot open on port : " + port);
        }
    }

    private synchronized void close() {
        this.hasStopped = true;

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        MyServer server = new MyServer(16425);
        new Thread(server).start();

        try {
            Thread.sleep(10500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Stopping Server");
        server.close();

    }
}
