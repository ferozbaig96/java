package com.kennycason.genetic.draw;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class MyWorkerRunnable implements Runnable {

    private String str;
    private Socket clientSocket;

    MyWorkerRunnable(Socket clientSocket, String msg) {
        this.clientSocket = clientSocket;
        this.str = msg;
    }

    @Override
    public void run() {
        OutputStream outputStream;
        try {
            outputStream = clientSocket.getOutputStream();
            outputStream.write(("OK\n\nMessage: " + str).getBytes());
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
