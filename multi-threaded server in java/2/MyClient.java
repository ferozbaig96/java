package com.kennycason.genetic.draw;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class MyClient {

    public static void main(String[] args) {
        try {
            Socket sock = new Socket(InetAddress.getLocalHost(), 16425);
            InputStream inputStream = sock.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = bufferedReader.readLine()) != null)
                System.out.println(line);

            sock.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
